// Завдання 7
// Доповніть код так, щоб він коректно працював

const array = ["value", () => "showValue"];

// Допишіть код тут

// alert(value); // має бути виведено 'value'
// alert(showValue()); // має бути виведено 'showValue'

const [elem1, elem2] = array;

console.log(elem1);
console.log(elem2());
